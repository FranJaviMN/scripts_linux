# SCRIPTS LINUX

Los scripts de linux nos va a permitir automatizar una gran cantidad de cosas que pueden ser muy tediosas ya que hoy dia un administrador de sistemas tiene que administrar una gran cantidad de maquinas asi que para ello usamos los scripts.

## ¿Que es un script de Linux

Un **script** en linux no es mas que un fichero de comandos que la consola interpreta y ejecuta. Este lenguaje es un **lenguaje interpretado**, es decir, que no es necesario que sea compilado para que sea ejecutado sino que la propia consola de comando va leyende de arriba hacia abajo y de izquierda a derecha hasta que encuentra el final del fichero.
Los scripts nos van a permitir automatizar tareas que son muy tediosas o muy pesadas, ademas podemos encontranos con muchos scripts tanto en el arranque/parada del sistema como en muchos otros programas que estan basados en scripts.

## Creando nuestro primer script

Para poder crear nuestros primeros scripts debemos tener las herramientas necesarias para poder crearlos.

* Lo primero que debemos hacer es abrirnos un fichero en texto plano en un editor.
* Para poder indicar que el fichero de texto plano tenemos que incluir en este una linea obligatoria llamada **Shebang**. El Shebang siempre va al principio del fichero de script y tiene el siguiente formato:

> #! [RUTA DEL INTERPRETE] (Ver interpretes disponible en /etc/shells)
>
En ete momento debemos tener mucho cuidado con las distribuciones de linux con las que estamos trabajando ya que, en muchas de ellas la ruta de los interpretes puede cambiar y no estar en la misma ruta que nosotros u otra persona ha puesto en el script en cuestion.

* Una vez hayamos puesto el interprete que vamos a usar solo debemos crear nuestro scripts dentro de ese fichero y al guardarlo se suele guardar con la extension **.sh** aunque no es necesario que se guarde en esa extension.

~~~bash
EJEMPLO DE SCRIPT LINUX
#!/usr/bin/python3
#Usamos el interprete de Python3

def saludar():
    print("Hola!")

saludar()
~~~

## Formas de ejecucion de scripts

Para poder ejecutar nuestros scripts en linux tenemos hasta 4 formas distintas de poder ejecutarlas:

* [NOMBRE INTERPRETE] nombre_script.

    Con este metodo de ejecucion debemos poner solo el interprete que vamos a usar, que debe ser el mismo interprete que se usa en el script. Para poder ejecutar el script no es necesario que dicho fichero donde tenemos nuestro script tenga permisos de ejecucion, sino que solo es necesario que tenga permisos de lectura ya que lo que hace nuestro interprete es, leer el contenido de dicho fichero e interpretarlo.
    Tambien debemos tener muy en cuenta es que, la forma de ejecutar el script medianete este proceso, lo ejecutar en una **subshell**.

* Especificando la ruta absoluta o relativa.

    Podemos hacerlo de tal forma que, solo especificando la ruta que tiene el fichero de script mediante la ruta, ya sea absoluta o relativa, se podra ejecutar. Hay que tener en cuenta que el fichero debe tener los permisos de ejecucion para que podamos ejecutarlo. Al igual que el metodo anteriro, este tambien ejecuta el script en una **subshell**.

* Comando **source**.

    Usamos el comando source junto al nombre de script, en esta forma de ejecutar los scripts no hace falta que le demos al fichero permisos de ejecucion. El script ejecutado mediante source no es como las anteriores formas de ejecucion de scripts que, al ser ejecutados, se ejecutaban en una subshell sino que cuando ejecutamos el script con source no nos ejecuta dicho script en una subshell, sino que es ejecutado en la misma shell donde nosotros nos encontramos.

* Comando **.**.

   Se usa el comando **.** y, al igual que usando el comando source, el fichero de script no va a necesitar permisos de ejecucion y tambien se ejecuta dicho script en la misma shell que nosotros trabajamos.

## Comentarios y lineas en blanco

En los scripts que vamos a crear, tanto en los programas que hagamos en otros lenguajes como los pequeños scripts que vayamos a crear, debemos explicar brevemente como es su funcionamiento, o al menos, las partes mas complivadas del script deberiamos explicarlas para que, las personas que vean ese script puedan entender su funcionamineto de una forma mucho mas liviana
