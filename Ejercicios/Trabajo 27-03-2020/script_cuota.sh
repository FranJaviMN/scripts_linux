#AUTOR: Francisco Javier Martin Nuñez
#CURSO: 1ºASIR
#CORREO: franjaviermn17100@gmail.com

#En esta funcion vamos a pedir al usuario que elija uno de los dispositivos de bloque que tiene en su maquina para poder sacar su UUID, que sera
#usado mas adelante en las funciones para crear cuotas. De momento solo sacamos el UUID del dispositivo elegido y lo guardamos.


function f_UUID {

lsblk -f
echo "¿Que dispositivo de bloques vas a utilizar?"
read bloque
echo "Se va a generar un fichero con el UUID del dispositivo de bloques seleccionado"
set `lsblk -io UUID /dev/"$bloque"` #Sacamos el uuid del dispositivo de bloque que vamos a usar

echo UUID="$2" > uuid.txt #Lo guardamos en el fichero de uuid.txt

}

#En esta funcion lo que logramos crear un fichero llamado fich_fstab.txt en el que podemos encontrar el bloque que el usuario a seleccionado anteriormente
#nos genera un fichero con esa estructura para, mas adelante, redireccionarlo al fichero de fstab



function f_modifica_fstab {

set `lsblk -f /dev/"$bloque" | egrep "$bloque"`

echo "Se va a crear una carpeta /QUOTA donde se montara el dispositivo de bloques"
sudo mkdir /QUOTA

while read uuid #Leemos el documento de uuid.txt crado anteirormente
	do echo -e "$uuid \t /QUOTA	 \t $2 \t defaults \t 0 \t 1" #generamos la estructura que tiene el fichero de fstab
done < uuid.txt > fich_fstab.txt #redireccionamos a un fichero con nombre fich_fstab.txt

cat fich_fstab.txt >> /etc/fstab #Inyectamos el contenido del fichero fich_fstab.txt a nuestro archivo de fstab

}

function f_habilita_quota {

	quotacheck –ugv /QUOTA
	sed `s/defaults/defaults,usrquota,grpquota` ./fich_fstab.txt
}