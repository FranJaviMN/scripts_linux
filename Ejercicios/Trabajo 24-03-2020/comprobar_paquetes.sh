#! /usr/bin/env

#AUTOR: Francisco Javier Martin Nuñez
#CURSO: 1ºASIR
#CORREO: franjaviermn17100@gmail.com

#En las sigiientes funciones vamos a hacer dos cosas a la misma vez, por una parte vamos a comprobar que los paquetes indicados en las funciones,
#es decir los paquetes de gdisk, parted, quota y quotatool, estan instalados en el sistema, para ello usaremos el binario de aptitude que, aunque 
#es parecido al bionario de apt este resuelve mejor las dependencias a la hora de instalar y desinstalar paquetes en nuestro sistema.
#Para ello debemos comprobar que el paquete de aptitude esta en el sistema.

#Comprobamos que el paquete de aptitude esta instalado, en caso contrario le pedimos al usuario instalarlo.

set `apt list aptitude`

if [ "$5" = "[instalado]" ] #Comprobamos el paquete si esta instalado
then
	echo "Paquete aptitude instalado, se puede continuar"
else
	echo "Para continuar se debe instalar el siguiente paquete: aptitude. ¿Quieres instalar el paquete aptitude?(y/n)"
	read opcion
	if [ "$opcion" = "y" ] #En caso de no estarlo se preguntara si se quiere instalar
	then
		sudo apt install  aptitude -y #Instala el paquete si le hemos dicho que queremos instalarlo
	else
		echo "No se puede continuar, saliendo..." #Si no lo instalamos no podremos continuar y se especifica porque.
	return "Fallo, se requiere instalar el paquete aptitude"
fi
fi



function f_instalar_quota {
#Comprobando que el paquete de quota esta instalado en el sistema

echo "Se va a comprobar que el paquete quota esta instalado en el sistema"

set `aptitude show quota | egrep "Estado"` #Hacemos la comprobacion con aptitude, de hay que necesitemos el paquete aptitude

if [ "$2" = "instalado" ] #Si cumple la condicion "instalado" se mostrara que ya esta instalado
then
	echo "El paquete quota esta instalado en el sistema"
else
	echo "El paquete quota no esta instalado, ¿Desea instalar el paquete quota?(y/n)"
	read opcion
	if [ "$opcion" = "y" ] #En caso de no estarlo se preguntara si se quiere instalar
	then
		sudo aptitude install quota -y #Instalamos el paquete mediante aptitude
	else
		echo "El paquete no se instalara"

	fi
fi
}

function f_instalar_quotatool {
#Comprobamos que el paquete quotatool esta instalado en el sistema

echo "Se va a comprobar que el paquete quotatool esta instalado en el sistema"

set `aptitude show quotatool | egrep "Estado"`

if [ "$2" = "instalado" ]
then
        echo "El paquete quotatool esta instalado en el sistema"
else
        echo "El paquete quotatool no esta instalado, ¿Desea instalar el paquete quotatool?(y/n)"
        read opcion
        if [ "$opcion" = "y" ]
        then
                sudo aptitude install quotatool -y
	else
		echo "No se instalara el paquete quotatool"

		fi
fi
}

function f_instalar_parted {
#Comprobamos que el paquete parted esta instalado en el sistema

echo "Se va a comprobar que el paquete parted esta instalado"

set `aptitude show parted | egrep "Estado"`

if [ "$2" = "instalado" ]
then
        echo "El paquete parted esta instalado en el sistema"
else
        echo "El paquete parted no esta instalado, ¿Desea instalar el paquete parted?(y/n)"
        read opcion
        if [ "$opcion" = "y" ]
        then
                sudo aptitude install parted -y
	else
		echo "No se instalara el paquete parted"

		fi
fi
}

function f_instalar_gdisk {
#comprobamos si gdisk esta instalado en el sistema

echo "Se va a comprobar que el paquete parted esta instalado"

set `aptitude show gdisk | egrep "Estado"`

if [ "$2" = "instalado" ]
then
        echo "El paquete gdisk esta instalado en el sistema"
else
        echo "El paquete gdisk no esta instalado, ¿Desea instalar el paquete gdisk?(y/n)"
        read opcion
        if [ "$opcion" = "y" ]
        then
                sudo aptitude install gdisk -y
	else
		echo "No se instalar el paquete gdisk"

		fi
fi
}

#En la siguiente funcion lo que vamos a hacer es, mediante el comando de gestion de disco y particiones sgdisk vamos a borrar la particion que el usuario
#introduzca por teclado. Antes se le mostrara con el comando lsblk la composicion de los discos que tiene en la maquina.

function f_borrar_tabla_particion {

lsblk -f

echo "Selecciona que tabla de particiones quiere borrar."

read disco

	sudo sgdisk -Z /dev/"$disco"	#Borramos el contenido del disco seleccionado

	lsblk -f 	#Muestra de comprobacion para mostrar que se ha borrado

}


#En la siguiente funcion vamos a crear, mediante el comando de gdisk, una particion que ocuper la totalidad del disco que el usuario seleccione por teclado.
#para ello vamos a seleccionar la opcion -N del comando sgdisk que crea una particion del tamaño total del disco que nosotros le indiquemos.

function f_crear_particion {

lsblk -f

echo "Se va a crear una particion del tamaño del disco. ¿Que en que disco quiere crear la particion?"

read particion

	sudo sgdisk -N 1 /dev/"$particion" #Creamos la particion en el disco seleccionado por el usuario previamente

	set `lsblk /dev/"$particion" | egrep "$particion"`

	yes | sudo mkfs.ext4 -L "CUOTA" /dev/"$1"1 #Creamo el sistema de archivos ext4 con mkfs, para automatizar usamos yes.

}


