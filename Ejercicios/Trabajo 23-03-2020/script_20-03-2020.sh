#! /usr/bin/env

#AUTOR = Francisco Javier Martín Núñez
#CURSO = 1ºASIR
#correo = franjaviermn17100@gmail.com

#En esta funcion vamos a concatenar la informacion de tres ficheros diferentes, para ello vamos a recorrer cada uno de ellos con un while seguido
#de un read en cada uno de los ficheros que vamos a leer, luego vamos a usar los descriptores 3 y 4 para poder usar mas de dos ficheros.
#Despues de haber hecho esto solo debemos imprimir por pantalla las variables en el orden que queramos y direccionar los ficheros a nuestro script.

function f_crea_fich_info {
while read nombre && read clave <&3 && read ip<&4

	do echo "$nombre:$clave:$ip"	#Imprimimos en el orden deseado

done < nombres.txt 3< claves.txt 4< ips.txt > fichero_info.txt	#Hacemos que la salida del comando se redireccione a un fichero.
}

#En esta funcion vamos a usar la funcion anterior, por lo que debemos ejecutarla antes de poder usar esta funcion, aunque en esta funcion se 
#ejecura la anterior, por lo que no hace falta que la ejecitemos nosotros.
#En esta funcion vamos a pedir por teclado al usuario que este usando este script, pueda buscar la ip mediante el apellido de los alumnos o los
#apellidos mediante la ip.


function f_asir_info {

#Cambiamos los separadores de set por :
oldfs=$IFS
IFS=":"

#Ejecutamos la funcion para que no sea necesario ejecutarla.
f_crea_fich_info

#Mostramos las opciones que tenemos
echo "---------------Menu----------------"
echo "1. Obtener IP mediante apellidos"
echo "2. Obtener Apellidos mediante IP"
echo "3. Salir del programa."
echo "Introduce la opcion: "

#Declaramos la variable para elegir las opciones
read opcion

#Usamos if para el control de las distintas opciones

if [ "$opcion" = "1" ] 
then
	echo "Dime los dos apellidos del alumno"
        read apellidos
        set `cat fichero_info.txt | egrep "$apellidos"`
        echo $3

elif [ "$opcion" = "2" ]
then
	echo "Dime la IP del alumno"
        read IP
        set `cat fichero_info.txt | egrep "$IP"`
        echo $1

elif [ "$opcion" = "3" ]
then
	echo "Saliendo del script..."
fi

#Volvemos a darle el valor que tenia por defecto
IFS="$oldfs"
}
