#! /usr/bin/env

#AUTOR: Francisco Javier Martín Núñez
#CURSO: 1ºASIR
#CORREO: franjaviermn17100@gmail.com

#En este scripts vamos a cambiar nuestros repositorios de http a https, suponiendo que necesitemos que nuestros repositorios trabajen sobre los
#repositorios de https, para ello vamos a usar el paquete de 'apt-transport-https'.

#ATENCION: El siguiente scripts trabajara con el superusuario. Si el usuario no dispone de la contraseña de superusuario no podra usar el script.

#En esta linea vamos a instalar el paquete apt-transport-https, en caso de estar instalado saltara esta opcion, sino, lo instalara de forma automatica

set `apt list aptitude`

if [ "$5" = "[instalado]" ] #Comprobamos si el paquete esta instalado.
then
	echo "Paquete apt-transport-https instalado, se puede continuar"
else
	echo "Paquete apt-transport-https no instalado, se procedera a instalar"
	apt install apt-transport-https -y
fi

#En este apartado cambiamos todos los http que esten en el fichero de source.list por https, para ello usamos el comando sed para reemplazar por
#lineas:

sed 's/http/https/' /etc/apt/sources.list > source.list
cat source.list > /etc/apt/source.list 
