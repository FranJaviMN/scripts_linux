#!/usr/bin/bash
#Usamos el interprete de bash.
#Lo que vamos a hacer es mostrar por pantalla el espacio que tenemos en la particion en la particion donde tengamos /home. En este caso sera
#sobre la particion sda9, que en mi caso, es donde tengo alojado el /home.

#Aqui sacamos los valores de espacio libre, usados y total.
set $(df -h | egrep 'sda9')  

#Mostramos por pantalla el mensaje con el tamaño de la particion, usado y libres.
echo "Tenemos $2 de espacio total, $3 usados y $4 libres."

